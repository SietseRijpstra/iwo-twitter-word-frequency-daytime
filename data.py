#!/usr/bin/python3
# This program does a little bit of pre-processing the tweets, and then counts the frequency of tweets for all words for all categories.
# It prints all frequencies, along with the total amount of tweets per category.
# Execute this program in a parent directory of the 'twitter' folder.
# by Sietse Rijpstra, s2952599

import os

# define categories and variables
morning = [6,7,8,9,10,11]
afternoon = [12,13,14,15,16,17]
evening = [18,19,20,21,22,23]
night = [0,1,2,3,4,5]

morningontbijt, morninglunch, morningdiner, afternoonontbijt, afternoonlunch, afternoondiner, eveningontbijt, eveninglunch, eveningdiner, nightontbijt, nightlunch, nightdiner = 0,0,0,0,0,0,0,0,0,0,0,0
morningtot,afternoontot,eveningtot,nighttot = 0,0,0,0

# get every file of the 'twitter' folder
for file in os.listdir("twitter"):
	f = open('twitter/{}'.format(file), 'r').readlines()
	diner = 0
	lunch = 0
	ontbijt = 0
	total = 0
    	
	for line in f:
		total += 1
		# pre-process the line
		line = line.lower().rstrip().split('\t')
		words = line[2].split()
		# add 1 to corresponding category
		if 'diner' in words:
			diner += 1
		elif 'lunch' in words:
			lunch += 1
		elif 'ontbijt' in words:
			ontbijt += 1

	# check if the last two digits of the filename appear in one of the categories
	# then add the totals to the corresponding category
	if int(file[9:]) in morning:
		morningontbijt += ontbijt
		morninglunch += lunch
		morningdiner += diner
		morningtot += total

	elif int(file[9:]) in afternoon:
		afternoonontbijt += ontbijt
		afternoonlunch += lunch
		afternoondiner += diner
		afternoontot += total

	elif int(file[9:]) in evening:
		eveningontbijt += ontbijt
		eveninglunch += lunch
		eveningdiner += diner
		eveningtot += total
	else:
		nightontbijt += ontbijt
		nightlunch += lunch
		nightdiner += diner
		nighttot += total

# output all frequencies with the total tweets in all categories
print(morningontbijt, morninglunch, morningdiner, morningtot)
print(afternoonontbijt, afternoonlunch, afternoondiner, afternoontot)
print(eveningontbijt, eveninglunch, eveningdiner, eveningtot)
print(nightontbijt, nightlunch, nightdiner, nighttot)

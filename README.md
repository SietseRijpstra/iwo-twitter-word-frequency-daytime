# Inleiding Wetenschappelijk Onderzoek Project - Placing terms in different time-of-day categories on Twitter
Steps to replicate this research.

1. Create a folder named 'twitter' (note the lower-case!) in the same directory as both python files.
2. Run gather_tweets.py while on a LWP computer, or while having access to the karora server. You should also be able to access /net/corpora.
3. Run data.py. You can choose to write the results to a file.
4. For every most frequent word in every category, set up a 2x2 contingency table with the following data:
	* Frequency of tweets with and without the term.
	* Frequency of tweets in the morning, and the rest of the day.
5. Run chi-square tests on the data from step 3.


Python version: 3.6.3


Python modules needed:

* os

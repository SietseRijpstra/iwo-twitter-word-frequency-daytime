#!/usr/bin/python3
# This program extracts all tweets from the twitter2 corpus in /net/corpora from the month March 2017.
# Using the tool tweet2tab, it only extracts the tweet ID, timestamp and tokenized text.
# Can only be executed on a LWP computer, or if you have access to the karora server.
# by Sietse Rijpstra, s2952599

import os 

count = 0
# Get a list of every file in the March 2017 twitter2 corpora
for file in os.listdir("/net/corpora/twitter2/Tweets/2017/03"):
	count += 1
	# send a command to linux that uses the tweet2tab tool to extract id, data and tokenized text from the twitter2 files
	command = "zless /net/corpora/twitter2/Tweets/2017/03/{} | /net/corpora/twitter2/tools/tweet2tab -i id date words > twitter/{}".format(file, file[:-7])
	os.system(command)
	# output progress to terminal
	print(count, file)


